import Phaser from "phaser";

export default class GameOverScene extends Phaser.Scene {
	constructor() {
		super({
			key: "GameOverScene",
		});
	}

	init() {}

	preload() {}

	create() {
		// this.scene.stop("DefaultScene");

		this.cameras.main.setBackgroundColor(0x212121);

		const { centerX, centerY, width } = this.cameras.main;

		const topBar = this.add.graphics();
		topBar.fillStyle(0x2196f3);
		topBar.fillRect(0, 0, width, 128);

		const gameOverText = this.add.text(0, 32, "GAME OVER", {
			fontFamily: "Montserrat",
			fontSize: 64,
		});

		gameOverText.setX(centerX - gameOverText.displayWidth / 2);

		const textStyle = {
			fontFamily: "Montserrat",
			fontSize: 24,
			align: "center",
		};

		const scoreTextStyle = {
			fontFamily: "Montserrat",
			fontSize: 36,
			color: "#2196f3",
			align: "center",
		};

		window.data.best = Math.max(window.data.score, window.data.best);

		const dist = 160;

		const progressBg = this.add.graphics();
		progressBg.fillStyle(0x363636);
		progressBg.fillRect(centerX - dist / 2, 240, dist, 8);

		const progressFill = this.add.graphics();
		progressFill.fillStyle(0x2196f3);
		progressFill.fillRect(
			centerX - dist / 2,
			240,
			dist * (window.data.score / window.data.best),
			8
		);

		const scoreTitleText = this.add.text(0, 180, "SCORE", textStyle);
		scoreTitleText.setX(centerX - dist - scoreTitleText.displayWidth / 2);

		const scoreText = this.add.text(
			0,
			220,
			window.data.score || 0,
			scoreTextStyle
		);
		scoreText.setX(centerX - dist - scoreText.displayWidth / 2);

		const bestTitleText = this.add.text(0, 180, "BEST", textStyle);
		bestTitleText.setX(centerX + dist - bestTitleText.displayWidth / 2);

		const bestText = this.add.text(
			0,
			220,
			window.data.best || 0,
			scoreTextStyle
		);
		bestText.setX(centerX + dist - bestText.displayWidth / 2);

		this.add.text(
			centerX - 134,
			centerY + 200,
			"press space to play again",
			{
				fontFamily: "Montserrat",
				fontSize: 24,
				color: "#505050",
			}
		);

		this.input.keyboard.once("keydown-SPACE", () => this.startGame());
	}

	update() {}

	startGame() {
		// this.scene.restart("DefaultScene");
		this.scene.start("DefaultScene");
	}
}
