import Phaser from "phaser";

export default class DefaultScene extends Phaser.Scene {
	playerSize = 32;

	startFallingGravity = 2000;
	startPlatformVelocity = 160;
	maxPlatformVelocity = 320;
	maxFallingGravity = 4000;

	platformThickness = 8;

	spaceBetweenPlatforms = this.playerSize + this.platformThickness * 2;

	constructor() {
		super({
			key: "DefaultScene",
		});
	}

	init() {}
	preload() {}

	create() {
		this.platforms = [];

		this.cameras.main.setBackgroundColor(0x212121);

		this.physics.world.gravity.y = 0;

		const { centerX, centerY } = this.cameras.main;

		// this.possiblePlatformPositions =
		// 	Math.floor(width / this.spaceBetweenPlatforms) - 1;

		this.possiblePlatformPositions = 8;
		this.platformOffset =
			centerX -
			this.spaceBetweenPlatforms *
				Math.floor(this.possiblePlatformPositions / 2);

		this.platformPhysicsGroup = this.physics.add.group({
			velocityY: this.startPlatformVelocity,
			immovable: true,
			allowGravity: false,
		});

		this.playerGraphic = this.add.graphics();
		this.playerGraphic.fillStyle(0x2196f3);
		this.playerGraphic.fillRect(0, 0, this.playerSize, this.playerSize);

		this.player = this.physics.add.existing(this.playerGraphic);
		this.player.body.setSize(this.playerSize, this.playerSize);

		// this.physicsGroup.add(this.player);

		this.player.x = centerX - this.playerSize / 2;
		this.player.y = centerY - this.playerSize / 2;

		this.gravityLeft = true;

		this.input.keyboard.on("keydown-SPACE", () => this.flipPlayer());

		this.player.body.setGravity(
			this.gravityLeft
				? -this.startFallingGravity
				: this.startFallingGravity,
			0
		);

		this.createPlatform(true);

		// this.playerIsOnPlatform = false;
		this.physics.add.collider(this.player, this.platformPhysicsGroup);

		this.scoreText = this.add
			.text(centerX, 32, "0", {
				fontFamily: "Montserrat",
				fontSize: 32,
			})
			.setStroke("#212121", 16);

		window.data.score = 0;
		this.startTime = this.time.now + 200;
	}

	createPlatform(first = false) {
		const { centerX, height } = this.cameras.main;

		const length = first ? height * 2 : 200;

		const platformSprite = this.add.graphics();
		platformSprite.fillStyle(0xfafafa);
		platformSprite.fillRect(0, 0, this.platformThickness, length);

		const platform = this.physics.add.existing(platformSprite, false);
		platform.body.setSize(this.platformThickness, length);

		platform.body.setImmovable(true);
		platform.body.allowGravity = false;
		// platform.body.setVelocityY(50);

		this.platformPhysicsGroup.add(platform);

		// this.physicsGroup.add(platform);

		platform.x = centerX - this.platformThickness / 2;

		if (first) {
			platform.x = this.platformOffset + 3 * this.spaceBetweenPlatforms;
		} else {
			platform.x =
				this.platformOffset +
				Math.floor(Math.random() * this.possiblePlatformPositions) *
					this.spaceBetweenPlatforms;

			platform.y = -length;
		}

		this.platforms.push(platform);
	}

	update() {
		const platformSpeed = Math.min(
			this.maxPlatformVelocity,
			this.startPlatformVelocity +
				Math.floor((this.time.now - this.startTime) / 64)
		);

		this.platformPhysicsGroup.setVelocityY(platformSpeed);

		// console.log(platformSpeed);

		const playerGravity = Math.min(
			this.maxFallingGravity,
			this.startFallingGravity +
				Math.floor(this.time.now - this.startTime)
		);

		// console.log(playerGravity);

		this.player.body.setGravity(
			this.gravityLeft ? -playerGravity : playerGravity,
			0
		);

		const { x, body } = this.player;
		const { width, height, centerY } = this.cameras.main;

		this.player.y = centerY - this.playerSize / 2;
		body.setVelocityY(0);

		if (x < -this.playerSize || x > width) {
			this.gameOver();
			// this.player.x = centerX - this.playerSize / 2;
			// body.setVelocityX(0);
		}

		let needNextPlatform = true;

		this.platforms = this.platforms.filter((platform) => {
			if (platform.y < 0) {
				needNextPlatform = false;
			}
			if (platform.y > height) {
				platform.destroy();
				return false;
			} else {
				return true;
			}
		});

		if (needNextPlatform) {
			this.createPlatform();
		}

		window.data.score = Math.floor((this.time.now - this.startTime) / 1000);
		this.scoreText.setText(window.data.score);

		// console.log(this.playerIsOnPlatform);
		// this.playerIsOnPlatform = false;
	}

	flipPlayer() {
		this.gravityLeft = !this.gravityLeft;

		// this.player.body.setVelocityX(0);

		if (this.player.body.velocity.x === 0)
			if (this.gravityLeft)
				this.player.x += this.platformThickness + this.playerSize;
			else this.player.x -= this.platformThickness + this.playerSize;
	}

	gameOver() {
		this.scene.start("GameOverScene");
	}
}
