import Phaser from "phaser";
import logoUrl from "../logo.png";

export default class MainMenuScene extends Phaser.Scene {
	constructor() {
		super({
			key: "MainMenuScene",
		});
	}

	init() {}

	preload() {
		this.load.image("logo", logoUrl);
	}

	create() {
		this.cameras.main.setBackgroundColor(0x212121);

		const { centerX, centerY } = this.cameras.main;

		const logo = this.add.image(centerX, centerY, "logo");
		logo.setScale(0.5);

		this.add.text(centerX - 100, centerY + 200, "press space to start");

		this.input.keyboard.once("keydown-SPACE", () => this.startGame());
	}

	update() {}

	startGame() {
		this.scene.start("DefaultScene");
	}
}
