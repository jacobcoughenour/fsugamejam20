import Phaser from "phaser";
import WebFont from "webfontloader";

export default class LoadingScene extends Phaser.Scene {
	constructor() {
		super({
			key: "LoadingScene"
		});
	}

	preload() {
		this.fontsReady = false;
		this.fontsLoaded = this.fontsLoaded.bind(this);

		const { centerX, centerY } = this.cameras.main;

		this.add.text(centerX - 80, centerY, "loading fonts...");

		WebFont.load({
			google: {
				families: ["Montserrat"]
			},
			active: this.fontsLoaded
		});
	}
	update() {
		if (this.fontsReady) {
			this.scene.start("MainMenuScene");
			// this.scene.start("GameOverScene");
		}
	}

	fontsLoaded() {
		this.fontsReady = true;
	}
}
