import DefaultScene from "./DefaultScene";
import GameOverScene from "./GameOverScene";
import LoadingScene from "./LoadingScene";
import MainMenuScene from "./MainMenuScene";

export const allScenes = [
	LoadingScene,
	MainMenuScene,
	DefaultScene,
	GameOverScene
];
