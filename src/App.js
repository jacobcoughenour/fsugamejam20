import "./App.css";

import React, { Component } from "react";

import Game from "./game";

class App extends Component {
	constructor(props) {
		super(props);

		this.gameElement = React.createRef();
	}

	componentDidMount() {
		this.game = new Game({
			width: 640,
			height: 640,
			physics: {
				default: "arcade",
				arcade: {
					debug: false,
					gravity: { y: 150 }
				}
			},
			parent: this.gameElement.current
		});
	}

	render() {
		return (
			<div className="App">
				<div ref={this.gameElement}></div>
			</div>
		);
	}
}

export default App;
