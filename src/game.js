import Phaser from "phaser";
import { allScenes } from "./scenes";

export default class Game extends Phaser.Game {
	constructor(gameConfig) {
		super(
			Object.assign(
				{
					width: 640,
					height: 640,
					type: Phaser.AUTO,
					scene: allScenes
				},
				gameConfig
			)
		);
	}
}
